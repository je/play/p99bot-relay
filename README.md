<h1><div align="center">Project 1999 Bot - Relay Container</div></h1>

[TOC]

# Overview

This container builds upon the Project 1999 Container[^2], adding
functionality to monitor EverQuest log files and relay lines
from those log files to a Redis server via the built-in Redis
publish-subscribe functionality.

# Creation

## Building the Container

For basic information about how to build these containers, see
the main project repo[^1].

The container is built from a Dockerfile which is based on the
Project 1999 Container image. This Dockerfile additionally installs
redis-cli and configures the default startup command (the same
`init` script as the Operating System Container).

# Operation

## Preparaion

For basic information about how to prepare a host to run these
containers, see the main project repo[^1].

This container is prepared essentially the same as the Project
1999 Container[^2] except the image referenced in the startup
script should change to refer to this container instead (i.e.,
"p99bot-relay-amd64") with the appropriate configuration parameters
(see the Configuration and Usage sections for more information).

## Requirements

The container requires a P99BOT_ROOT environment variable to be
set. During normal startup this is set by the initialization script
prior to running the other `rc.d` scripts.

This container requires an external Redis server and an account
on that server with permission to execute the auth and publish
commands.

## Configuration

The container is configured by setting environment variables when
running the container (e.g. via the `-e` switch when using the
`docker run` command). Available environment variables are:

* **P99BOT_RELAY_DISABLED**: If set to any value, the relay will
  be disabled. By default this variable is not set.
* **P99BOT_RELAY_LOG_MONITOR**: If set to any value a the log
  monitor script will start on initialization. This script will
  restart the relay when a new EverQuest log file appears. This
  requires slightly more resources as it runs an additional
  process to watch the log folder. By default this variable is
  not set.
* **P99BOT_RELAY_REDIS_CHANNEL**: The Redis channel to which all
  messages will be published. The default value is "p99".
* **P99BOT_RELAY_REDIS_USERNAME**: The username to use when
  authenticating with the Redis server. There is no default value.
* **P99BOT_RELAY_REDIS_PASSWORD**: The password to use when
  authenticating with the Redis server. There is no default value.
* **P99BOT_RELAY_REDIS_HOSTNAME**: The Redis server hostname.
  There is no default value.
* **P99BOT_RELAY_REDIS_HOSTPORT**: The Redis server port. This
  variable is optional; the default value is 6379.

## Usage

For basic information about how to use this container, see the
main project repo[^1].

Here is an example command using typical required settings:

    sudo docker run -p 8080:8080 \
      -e P99BOT_P99_XPRA_PASSWORD=test \
      -e P99BOT_RELAY_LOG_MONITOR=true \
      -e P99BOT_RELAY_REDIS_USERNAME=p99 \
      -e P99BOT_RELAY_REDIS_PASSWORD=mypass \
      -e P99BOT_RELAY_REDIS_HOSTNAME=1.2.3.4 \
      p99bot-relay-amd64

## Notes

If the WSL_DISTRO_NAME environment variable is set, which should
only happen in a Windows WSL environment, the command that watches
the log files will be run with a "timeout" command wrapper. This
is because WSL doesn't properly support inotify so we can't detect
when log files change, which is needed to restart the relay.
Instead, we periodically restart it via a timer instead. On real
Linux we can tell when a file gets created, which should indicate
a new character has logged in, and restart the tail command at
that time.

Currently, the redis-cli command doesn't validate TLS certificates.
It only uses TLS for encryption, not authentication.

# To-Do

* I don't remember if `start-relay-monitor` actually works, it doesn't look like it would; test this out again on both WSL and Ubuntu hosts.

---

[^1]: https://gitlab.com/je/play/p99bot

[^2]: https://gitlab.com/je/play/p99bot-p99
